/*
Copyright (c) 2020-2021, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package main

import (
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/iatlevsha/goto/pkg/store"
	"gitlab.com/iatlevsha/goto/pkg/utils"
)

type httpHandler struct {
	store *store.Store
}

func (h *httpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Debugf("Request: %s\n", r.URL)
	keywords := utils.Splits(strings.Trim(r.URL.Path, "/"), []string{"/", "-"})
	switch res := h.store.Search(keywords); len(res) {
	case 0:
		fmt.Fprint(w, "No results")
	case 1:
		http.Redirect(w, r, res[0], http.StatusFound)
	default:
		fmt.Fprint(w, "<html><body>Multiple results found:\n")
		for _, host := range res {
			fmt.Fprintf(w, "<br><a href='%s'>%s</a>\n", host, host)
		}
		fmt.Fprint(w, "</body></html>")
	}
}

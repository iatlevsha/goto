
gofiles := $(wildcard version/*.go cmd/*/*.go pkg/*/*.go)

VERSION	:= $(shell scripts/version.sh)
TAG	:= $(shell ./scripts/version.sh|sed -e 's/[ :]/_/g')

.PHONY: all
all: build

.PHONY: build
build: bin/goto

bin/goto: $(gofiles)
	mkdir -p bin
	CGO_ENABLE=1 go build -o bin -race -ldflags "-extldflags '-static' -X 'gitlab.com/iatlevsha/goto/version.Version=$(VERSION)'" ./...

.PHONY: test
test:
	go fmt $(shell go list ./... | grep -v /vendor/)
	go vet $(shell go list ./... | grep -v /vendor/)
	mkdir -p coverage
	go test -race -coverprofile=coverage/coverage.out ./...
	go tool cover -html=coverage/coverage.out -o coverage/coverage.html
	go tool cover -func coverage/coverage.out

.PHONY: dockerimage
dockerimage: Dockerfile
	docker build --tag "goto:latest" --tag "goto:${TAG}" --build-arg VERSION="${VERSION}" .

.PHONY: run-in-docker
run-in-docker: dockerimage
	docker run -it --rm -p 8080:8080 -v $(PWD)/deployments/kubernetes/example/cfg:/cfg goto:${TAG} -hostsfile /cfg/hosts.txt

.PHONY: clean
clean:
	-rm -rf bin coverage

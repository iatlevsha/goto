# goto

The service to show or redirect to domains that have keywords requested.

## Goal

The usage scenario of the service is to help navigate over a (potentially big but limited) set of websites.
This is a pretty common scenario when you have several internal, corporate etc resources that you need to access regularly or occasionally.
The popular attempt to make this task easier is to use a special structure of domains, like \<service\>.\<namespace\>.\<cluster\>.\<company\>.tld.
This structure does not always help remembering these sites however ("Does "namespace" come before "cluster" or after?",
"How exactly this service is named in that cluster?", "Where this service runs?" etc).

The "goto" service accepts the set of keywords as the url path and returns websites that contain these keywords in their URLs.
When the only one site matches all these keywords then the service will redirect to this site immediately.
The order of keywords does not matter.

## Usage example.

Imagine you have "prometheus" and "grafana" services deployed in "prod" and "dev" namespaces in several datacenters.
Then you can do these requests:

* http://goto/grafana/ - will show you all instances of grafana deployed
* http://goto/xyz/prod/ - will show you all services deployed in the namespace "prod" in the cluster "xyz"
* http://xyz/prometheus/dev - will redirect you to the "prod" prometheus instance deployed in the cluster "xyz"

## Deploy.

The set of sites has to be pre filled into the file that will be passed to the app in the command line.
An option how to build the set of the sites based on Ingress objects from k8s clusters:

```bash
kubectl config get-contexts --output=name | xargs -I % kubectl --context=% get ingress -A -o jsonpath='{range .items[*].spec.rules[*]}{.host}{"\n"}{end}' | sort -u > hosts.txt
```

The service should be deployed to be available over http and/or https.

It can be also deployed to be available by short link (like "goto" or just "go"):

* add "goto.\<company\>.tld" DNS record to point to the service
* add "\<company\>.tld" to the "SEARCH" list of workstations (manually or using dhcp or vpn).

[The code](https://gitlab.com/iatlevsha/goto/-/tree/master/deployments/kubernetes) contains the base and an example how to deploy the service into k8s cluster.

# LICENSE

Copyright (c) 2020-2021, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.

/*
Copyright (c) 2020-2021, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package store

import (
	"net/url"

	"github.com/goware/urlx"

	"gitlab.com/iatlevsha/goto/pkg/utils"
)

type host struct {
	url      *url.URL
	keywords map[string]bool
}

type Store struct {
	hosts []host
}

type WrongHost struct {
	host string
	err  error
}

func NewStore(hosts []string) (*Store, []WrongHost) {
	s := Store{}
	w := make([]WrongHost, 0)
	for _, in := range hosts {
		u, err := urlx.Parse(in)
		if err != nil {
			w = append(w, WrongHost{in, err})
			continue
		}
		h := host{url: u}
		h.keywords = make(map[string]bool)
		for _, k := range utils.Splits(u.Host, []string{".", "-", ":"}) {
			h.keywords[k] = true
		}
		for _, k := range utils.Splits(u.Path, []string{"/", "-", "_"}) {
			h.keywords[k] = true
		}
		s.hosts = append(s.hosts, h)
	}
	return &s, w
}

func (s *Store) Search(keywords []string) (ret []string) {
	hosts := s.hosts
	for _, k := range keywords {
		hosts = filter(hosts, k)
	}
	for _, h := range hosts {
		ret = append(ret, h.url.String())
	}
	return
}

func filter(hosts []host, keyword string) (ret []host) {
	for _, h := range hosts {
		if h.keywords[keyword] {
			ret = append(ret, h)
		}
	}
	return
}

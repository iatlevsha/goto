/*
Copyright (c) 2020, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package main

import (
	"bufio"
	"os"
)

func hostsFromFile(filename string) (hosts []string, err error) {
	f, err := os.Open(filename)
	if err != nil {
		return
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		hosts = append(hosts, scanner.Text())
	}
	if err = scanner.Err(); err != nil {
		return
	}
	return
}

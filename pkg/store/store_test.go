/*
Copyright (c) 2020-2021, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package store

import (
	"io"
	"os"
	"reflect"
	"testing"

	"gopkg.in/yaml.v2"
)

type TestCase struct {
	Name     string
	Hosts    []string
	Keywords []string
	Want     []string
}

func TestSearch(t *testing.T) {
	f, e := os.Open("test_cases.yaml")
	if e != nil {
		t.Fatalf("Error opening test_cases.yaml: %v", e)
	}
	defer f.Close()
	testsDecoder := yaml.NewDecoder(f)
	var cc []TestCase
	for {
		switch e := testsDecoder.Decode(&cc); e {
		case io.EOF:
			return
		case nil:
		default:
			t.Fatalf("Error reading test case: %v", e)
		}
		for _, c := range cc {
			st, _ := NewStore(c.Hosts)
			got := st.Search(c.Keywords)
			if !reflect.DeepEqual(got, c.Want) {
				t.Errorf("test %v:\ngot : %v\nwant: %v", c.Name, got, c.Want)
			}
		}
	}
}

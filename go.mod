module gitlab.com/iatlevsha/goto

go 1.16

require (
	github.com/goware/urlx v0.3.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/objx v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.3.0
)

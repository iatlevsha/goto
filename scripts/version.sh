#!/bin/sh

set -e

COMMIT=$(git rev-parse HEAD)

TAG=$(git describe --exact-match --abbrev=0 --tags ${COMMIT} 2> /dev/null || true)
if [ -n "${TAG}" ]
then
  VERSION="${TAG}"
else
  VERSION="git:${COMMIT}"
fi

if [ $(git diff HEAD|wc -l) -gt 0 ]
then
  if which md5 > /dev/null
  then
    MD5="$(git diff HEAD|md5)"
  elif which md5sum > /dev/null
  then
    MD5="$(git diff HEAD|md5um)"
  else
    echo "Can't calculate md5 from diff" >&2
    exit 1
  fi
  VERSION="${VERSION} md5-diff:${MD5}"
fi
echo "${VERSION}"

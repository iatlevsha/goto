FROM golang:1.16.12-bullseye as build

ARG VERSION

WORKDIR /src
COPY ./ ./
ENV CGO_ENABLED=0

RUN go build -o /bin\
       -ldflags "-extldflags '-static' -X 'gitlab.com/iatlevsha/goto/version.Version=${VERSION}'" ./...

FROM gcr.io/distroless/base-debian11 AS final

USER nonroot:nonroot
COPY --from=build /bin/goto /bin/goto

EXPOSE 8080

ENTRYPOINT ["/bin/goto"]


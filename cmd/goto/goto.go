/*
Copyright (c) 2020-2021, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/iatlevsha/goto/pkg/logflags"
	"gitlab.com/iatlevsha/goto/pkg/store"
	"gitlab.com/iatlevsha/goto/version"

	log "github.com/sirupsen/logrus"
)

var (
	hostsFile   = flag.String("hostsfile", "", "File name that contains list of hosts, one per line")
	port        = flag.Uint("port", 0, "Port to listen on. Default is the value of 'PORT' environment variable or 8080 if the variable is not set")
	versionFlag = flag.Bool("version", false, "Get version")

	logLevel logflags.LogLevel = logflags.LogLevel(log.InfoLevel)

	listenAddr = ":8080"
)

func init() {
	log.SetFormatter(&log.TextFormatter{DisableTimestamp: true})
}

func main() {
	flag.Var(&logLevel, "loglevel", "Log level. Can be Trace, Debug, Info, Warning, Error, Fatal and Panic")
	flag.Parse()
	log.SetLevel(log.Level(logLevel))
	if *versionFlag {
		fmt.Printf("Version: %v\n", version.Version)
		os.Exit(0)
	}
	if len(*hostsFile) == 0 {
		log.Errorf("hostsfile is not set")
		flag.PrintDefaults()
		os.Exit(1)
	}
	hosts, err := hostsFromFile(*hostsFile)
	if err != nil {
		log.Fatalf("Error loading hosts from file '%v': %v", *hostsFile, err)
	}

	if envPort := os.Getenv("PORT"); envPort != "" {
		listenAddr = fmt.Sprintf(":%s", envPort)
	}
	if *port > 0 {
		listenAddr = fmt.Sprintf(":%d", *port)
	}
	st, e := store.NewStore(hosts)
	if len(e) > 0 {
		log.Errorf("Ignored hosts that was not able to parse: %v", e)
	}
	http.Handle("/", &httpHandler{store: st})
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
	})
	log.Printf("Listening on %v", listenAddr)
	log.Fatal((&http.Server{Addr: listenAddr}).ListenAndServe())
}

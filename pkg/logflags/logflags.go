/*
Copyright (c) 2020, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
// Packages logflags implements types to configure logrus using command line flags.
package logflags

import "github.com/sirupsen/logrus"

type LogLevel logrus.Level

func (v *LogLevel) String() string {
	if v != nil {
		return logrus.Level(*v).String()
	}
	return ""
}

func (v *LogLevel) Set(s string) error {
	l, err := logrus.ParseLevel(s)
	if err != nil {
		return err
	}
	*v = LogLevel(l)
	return nil
}

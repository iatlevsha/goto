/*
Copyright (c) 2020, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package utils

import "strings"

func Splits(in string, seps []string) (out []string) {
	if len(seps) < 1 {
		return append(out, in)
	}
	for _, item := range Splits(in, seps[1:]) {
		out = append(out, strings.Split(item, seps[0])...)
	}
	return out
}

/*
Copyright (c) 2020-2021, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/iatlevsha/goto/pkg/store"
)

var hosts = []string{
	"www.google.com",
	"intel.com",
	"who.int",
	"http://www.some-site.local:5050/",
	"https://secure-site.io",
	"https://blog.levsha.me/tags/goto/",
}

var testCases = []struct {
	in       string
	wantCode int
	want     string
}{
	{
		"/www/google",
		302,
		`<a href="http://www.google.com">Found</a>.`,
	},
	{
		"/google/",
		302,
		`<a href="http://www.google.com">Found</a>.`,
	},
	{"/notexists", 200, "No results"},
	{
		"/com",
		200,
		`<html><body>Multiple results found:
<br><a href='http://www.google.com'>http://www.google.com</a>
<br><a href='http://intel.com'>http://intel.com</a>
</body></html>`,
	},
}

func TestHander(t *testing.T) {
	for _, tt := range testCases {
		req, err := http.NewRequest("GET", tt.in, nil)
		if err != nil {
			t.Fatalf("http.NewRequest() returned an error: %v", err)
		}
		rr := httptest.NewRecorder()

		st, _ := store.NewStore(hosts)
		(&httpHandler{store: st}).ServeHTTP(rr, req)
		if gotCode := rr.Code; gotCode != tt.wantCode {
			t.Errorf("httpHandler.ServeHTTP(\"%v\") returned wrong code:\n got: %v\nwant: %v", tt.in, gotCode, tt.wantCode)
		}
		if got := strings.TrimSpace(rr.Body.String()); got != tt.want {
			t.Errorf("httpHandler.ServeHTTP(\"%v\") returned wrong data:\ngot:\n%v\nwant:\n%v", tt.in, got, tt.want)
		}
	}
}

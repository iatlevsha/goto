# Changelog
- Switch to use distroless as base image.
- Support URLs with protocol and/or port set.
- Search in the path.
## [v0.2.1] - 2020-10-26
- Fix kustomize example.
## [v0.2.0] - 2020-10-26
- Make it public.
- Use logrus logging library.
- Redirect if only single host found.
## [v0.1.0] - 2020-10-02
- Initial release.

/*
Copyright (c) 2020, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package main

import (
	"reflect"
	"testing"
)

func TestErrorOnNoFile(t *testing.T) {
	_, err := hostsFromFile("file_does_not_exist.txt")
	if err == nil {
		t.Error("Loading not existing file:\ngot : no error\nwant: an error")
	}
}

func TestReadingHostsFromFile(t *testing.T) {
	got, err := hostsFromFile("../../deployments/kubernetes/example/cfg/hosts.txt")
	if err != nil {
		t.Fatalf("Error loading hosts from file 'hosts.txt': %v", err)
	}
	want := []string{
		"microsoft.com",
		"intel.com",
		"www.google.com",
		"gmail.com",
		"who.int",
		"whitehouse.gov",
		"https://blog.levsha.me/tags/blog/",
	}
	if !reflect.DeepEqual(got, want) {
		t.Errorf("Loading 'hosts.txt':\ngot : %v\nwant: %v", got, want)
	}

}

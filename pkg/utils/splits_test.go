/*
Copyright (c) 2020, Mykola Dzham
All rights reserved.

This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree.
*/
package utils

import (
	"reflect"
	"testing"
)

var testCases = []struct {
	in   string
	seps []string
	want []string
}{
	{
		"www.who.int",
		[]string{"."},
		[]string{"www", "who", "int"},
	},
	{
		"www.this-site.com",
		[]string{"."},
		[]string{"www", "this-site", "com"},
	},
	{
		"www.that-site.com",
		[]string{".", "-"},
		[]string{"www", "that", "site", "com"},
	},
}

func TestSplits(t *testing.T) {
	for _, tt := range testCases {
		got := Splits(tt.in, tt.seps)
		if !reflect.DeepEqual(got, tt.want) {
			t.Errorf("Splits(%v, %v) returned wrong data:\n got: %v\nwant:%v", tt.in, tt.seps, got, tt.want)
		}
	}
}

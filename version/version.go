// Package contains Version variable that the build will set.
package version

// The build process sets the value of this variable using "-ldflags '-X package.module.Variable=value'"
// It has to be variable, not constant.
var Version = "devel"
